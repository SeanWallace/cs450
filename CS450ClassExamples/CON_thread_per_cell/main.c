//
//  main.c
//  CON_thread_per_cell
//
//  Created by Sean Wallace on 2/17/16.
//  Copyright © 2016 Sean Wallace. All rights reserved.
//

#include <stdio.h>
#include <pthread.h>

#define DIM 50

int	A[DIM][DIM],	/* src matrix A */
B[DIM][DIM],	/* src matrix B */
C[DIM][DIM];	/* dest matrix C */

void *row_dot_col(void *index) {
	int *pindex = (int *)index;
	int i = pindex[0];
	int j = pindex[1];
	
	//printf("Hello from i = %d, j = %d, thread id = %d.\n", i, j, (int)pthread_self());
	
	C[i][j] = 0;
	
	for (int x = 0; x < DIM; x++) {
		C[i][j] += A[i][x] * B[x][j];
	}
	
	pthread_exit(NULL);
}

void run_with_thread_per_cell() {
	pthread_t ptd[DIM][DIM];
	int index[DIM][DIM][2];
	
	for (int i = 0; i < DIM; i++) {
		for (int j = 0; j < DIM; j++) {
			index[i][j][0] = i;
			index[i][j][1] = j;
			
			pthread_create(&ptd[i][j], NULL, row_dot_col, index[i][j]);
		}
	}
	
	for (int i = 0; i < DIM; i++) {
		for (int j = 0; j < DIM; j++) {
			pthread_join(ptd[i][j], NULL);
		}
	}
}

int main(int argc, const char * argv[]) {
	run_with_thread_per_cell();
	
    return 0;
}
