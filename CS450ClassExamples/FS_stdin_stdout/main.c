//
//  main.c
//  FS_stdin_stdout
//
//  Created by Sean Wallace on 11/3/15.
//  Copyright © 2015 Sean Wallace. All rights reserved.
//

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

void ex1(int argc, const char * argv[]) {
	int fd = open("foo.txt", O_CREAT|O_TRUNC|O_RDWR, 0644);
	dup2(fd, 1); /* set fd 1 (stdout) to be "foo.txt" */
	printf("Arg: %s\n", argv[1]);
}

void ex2(int argc, const char *argv[]) {
	int fd = open("foo.txt", O_CREAT|O_TRUNC|O_RDWR, 0644);

	if (fork() == 0) {
		dup2(fd, 1);
		execlp("echo", "echo", "Hello!", NULL);
	}
	close(fd);
}


int main(int argc, const char * argv[]) {
	ex2(argc, argv);
}
