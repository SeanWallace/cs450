CS 450 Repository
=================

CS 450 students will find all handouts and example source code here.

To clone the repository, use the command:
    git clone https://SeanWallace@bitbucket.org/SeanWallace/cs450.git

This will create a local git repository for you in 'cs450' under your current
directory and automatically create and check out an initial branch named
'master' based on the upstream repository's master branch (maintained by me).

Git has a comprehensive online help system that you can access with the commands
`git help` and `git help SOME_COMMAND`.

Feel free to contact me if you're in my class and you need help setting up
and/or using git.

Enjoy!
