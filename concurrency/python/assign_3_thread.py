from threading import Thread
from time import sleep
from random import random

def say_hello(id):
	# sleep(random()) # Introduce a little random lag to each thread to make non-deterministic
	print('hello from child', id)

for i in range(5):
	t = Thread(target=say_hello, args=[i])
	t.start()
	print('started thread', t.ident)