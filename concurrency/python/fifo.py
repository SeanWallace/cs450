from __future__ import print_function
from threading import Semaphore, Thread
import threading
from random import random
from time import sleep
from collections import deque
import sys

class FifoSem:
    def __init__(self, val):
        self.val = val
        self.queue = deque()
        self.mutex = Semaphore(1)

    def acquire(self):
        barrier = Semaphore(0) # thread local semaphore
        block = False
        tid = threading.current_thread().ident
        idx = threading.current_thread().getName()
        with self.mutex:
            self.val -= 1
            if self.val < 0:
                self.queue.append((barrier, tid, idx))
                block = True
                print('t{} ({}) queued request'.format(tid, idx))
        if block:
            barrier.acquire()

    def release(self):
        with self.mutex:
            self.val += 1
            if self.queue:
                (barrier, tid, idx) = self.queue.popleft()
                print('Waking up t{} ({})'.format(tid, idx))
                barrier.release()

    # the following methods support the "with" statement
    def __enter__(self):
        self.acquire()
    def __exit__(self, exc_type, exc_value, traceback):
        self.release()

def tbody(mutex):
    with mutex:
        tid = threading.current_thread().ident
        idx = threading.current_thread().getName()
        print('t{} ({}) in critical section'.format(tid, idx))
        sleep(1)

if __name__ == '__main__':
    fifo = FifoSem(1)
    for i in range(10):
        t = Thread(target=tbody, args=[fifo])
        t.setName(i)
        t.start()
        sleep(0) # yield to new thread
