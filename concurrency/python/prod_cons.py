from __future__ import print_function
from threading import Semaphore, Thread, Lock
from collections import deque
from random import random
from time import sleep
import sys
import logging

class Queue:
    def __init__(self):
        self.head = self.tail = None

    def dequeue(self):
        if self.head is not None:
            rv = self.head[0]
            self.head = self.head[1]

            if self.head is None:
                self.tail = None

            return rv
        else:
            raise Exception('Dequeue on empty queue')

    def enqueue(self, val):
        if self.tail is not None:
            self.tail[1] = [val, None]
            sleep(random())
            self.tail = self.tail[1]
        else:
            self.head = self.tail = [val, None]

    def __repr__(self):
        vals = []
        q = self.head
        while q is not None:
            vals.append(repr(q[0]))
            q = q[1]
        return '[' + ', '.join(vals) + ']'

spaces  = Semaphore(5)
items   = Semaphore(0)
mutex   = Lock() # Equivalent to Semaphore(1)
buf     = Queue()

def producer_no_sync(n):
    for i in range(0,n):
        buf.enqueue(i)
        sleep (random()/150.0)

def consumer_no_sync(n):
    for i in range(0,n):
        print(buf)
        item = buf.dequeue()
        print(item)
        sleep(random()/100.0)

def producer(n):
    for i in range(0, n):
        spaces.acquire()
        # With keyword in Python automatically calles entry and exit functions.
        with mutex:
            buf.enqueue(i)
        items.release()
        #sleep(random())

def consumer(n):
    for i in range(0,n):
        items.acquire()
        with mutex:
            print(buf)
            item = buf.dequeue()
        spaces.release()
        print(item)
        # sleep(random() / 100)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Not enough arguments!")
        sys.exit()

    n = int(sys.argv[1])

    if len(sys.argv) > 2 and sys.argv[2] == '--sync':
        prod_fn = producer
        cons_fn = consumer
    else:
        prod_fn = producer_no_sync
        cons_fn = consumer_no_sync

    p_t = Thread(target = prod_fn, args = [n])
    p_t.start()
    c_t = Thread(target = cons_fn, args = [n])
    c_t.start()
