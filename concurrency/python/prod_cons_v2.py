from __future__ import print_function
from threading import Thread
from queue import Queue
from time import sleep

NUM_ITEMS = 10

def producer():
    for i in range(NUM_ITEMS):
        print("Producer putting {}".format(i))
        q.put(i)

def consumer():
    for i in range(NUM_ITEMS):
        item = q.get(i)
        print("Consumer processing {}".format(item))
        sleep(1)
        q.task_done()

if __name__ == '__main__':
    q = Queue(5)
    p = Thread(target=producer)
    c = Thread(target=consumer)
    p.start()
    c.start()
    q.join()
    print("All items processed!")
