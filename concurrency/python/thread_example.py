import sys
from random import randint
from threading import Thread

def thread_body(thread_id):
    garbage = 0
    upper_bound = 10000000
    for i in range(0, upper_bound):
        if (i % (upper_bound / 10) == 0) :
            print("Thread {} {}% done.".format(thread_id, (i / upper_bound) * 100))
        garbage += randint(0,9)

    print("Thread {} 100% done.".format(thread_id))

if __name__ == '__main__':
    num_threads = 2

    for i in range(0, num_threads):
        thread = Thread(target=thread_body, args = [i])
        thread.start()
        print("Thread {} started.".format(i))